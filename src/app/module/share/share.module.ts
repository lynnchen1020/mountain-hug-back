import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ModalNewComponent } from 'src/app/components/modal/modal-new.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ConfirmBoxComponent } from 'src/app/components/modal/confirm-box/confirm-box.component';
import { MemoComponent } from '../../components/common/memo/memo.component';
import { FormsModule } from '@angular/forms';
import { NavBarComponent } from 'src/app/components/nav-bar/nav-bar.component';
import { OrderDetailsComponent } from 'src/app/components/modal/order-details/order-details.component';
import { DatePickerComponent } from 'src/app/components/date-picker/date-picker.component';
import { DefaultConfirmBoxComponent } from 'src/app/components/modal/default-confirm-box/default-confirm-box.component';

const components = [
  DatePickerComponent,
  ModalNewComponent,
  ConfirmBoxComponent,
  MemoComponent,
  NavBarComponent,
  OrderDetailsComponent,
  DefaultConfirmBoxComponent
]


@NgModule({
  declarations: [...components],
  imports: [
    FontAwesomeModule,
    FormsModule,
    RouterModule,
    CommonModule,
  ],
  exports: [...components, FontAwesomeModule],
})
export class ShareModule { }
