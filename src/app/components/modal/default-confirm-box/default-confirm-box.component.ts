import { Component, Input } from '@angular/core';
import { ModalNewService } from 'src/app/services/modal-new.service';
import { ModalNewBase } from '../modal-new-base';
import { ToasterComponent } from '../toaster/toaster.component';

@Component({
  selector: 'app-default-confirm-box',
  templateUrl: './default-confirm-box.component.html',
  styleUrls: ['./default-confirm-box.component.sass']
})
export class DefaultConfirmBoxComponent extends ModalNewBase {
  modal: any = null;
  @Input() props!: {
    title: string;
    msg: string;
    order_id: string;
    order_name: string;
  };
  isSubmitted = false;

  constructor(
    modalNew: ModalNewService,
  ) {
    super(modalNew)
  }

  ngOnDestroy(): void {
    this.isSubmitted = false;
  }

  clickSubmit() {
    this.isSubmitted = true;

    if(this.modal) {
      this.modal.close();
    }
    this.modal = this.modalNew.open(ToasterComponent, {
      msg: '已送出'
    })
  }

}
