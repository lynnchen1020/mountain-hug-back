import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultConfirmBoxComponent } from './default-confirm-box.component';

describe('DefaultConfirmBoxComponent', () => {
  let component: DefaultConfirmBoxComponent;
  let fixture: ComponentFixture<DefaultConfirmBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefaultConfirmBoxComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DefaultConfirmBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
