import { Component, ComponentFactoryResolver, ComponentRef, OnDestroy, OnInit, ViewChild, ViewContainerRef, ViewRef } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { ModalNewService } from 'src/app/services/modal-new.service';

@Component({
  selector: 'app-modal-new',
  template: '<ng-container #container></ng-container>',
})
export class ModalNewComponent implements OnInit, OnDestroy {

  @ViewChild('container', { read: ViewContainerRef, static: true })
  container!: ViewContainerRef;

  subscribeList: Subscription[] = [];
  componentRef!: ComponentRef<any>;
  routerChange!: Subscription;

  constructor(
    public resolver: ComponentFactoryResolver,
    private modalNew: ModalNewService,
    public router: Router,
  ) {}

  ngOnInit() {
    this.bindModalEvents();
    this.routerChange = this.router.events
    .pipe(filter((evt) => evt instanceof NavigationEnd))
    .subscribe(() => {
      this.container.clear();
      this.modalNew.modalList = [];
    });
  }

  ngOnDestroy(): void {
    if(!!this.componentRef) {
      this.componentRef.destroy();
    }

    this.subscribeList.forEach(sb => sb.unsubscribe());
    if(this.routerChange){
      this.routerChange.unsubscribe();
    }
  }

  bindModalEvents() {
    const list = [];

    list[0] = this.modalNew.onComponentCreate.subscribe(data => {
      if (data) {
        const { component, props } = data;
        const factory = this.resolver.resolveComponentFactory(component);

        this.componentRef = this.container.createComponent(factory);
        this.componentRef.instance.props = props;
        this.componentRef.instance.hostView = this.componentRef.hostView;

        this.modalNew.modalList.push(this.componentRef.instance);
      }
    });

    list[1] = this.modalNew.onComponentCloseLast.subscribe(() => {
      this.container.remove();

      this.modalNew.modalList.pop();
    });

    list[2] = this.modalNew.onComponentClearAll.subscribe(() => {
      this.container.clear();

      this.modalNew.modalList = [];
    });

    list[3] = this.modalNew.onComponentCloseByHostView.subscribe((hostView: ViewRef) => {
      this.closeComponent(hostView);
    });

    this.subscribeList.concat(list);
  }

  getViewIndex(hostView: ViewRef) {
    return this.container.indexOf(hostView);
  }

  closeComponent(hostView: ViewRef) {
    const i = this.getViewIndex(hostView);

    if (i < 0) {
      return;
    }

    this.container.remove(i);

    this.modalNew.modalList = this.modalNew.modalList.filter(
      item => item.hostView !== hostView
    );
  }

}
