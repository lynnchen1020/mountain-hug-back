import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ModalNewService } from 'src/app/services/modal-new.service';
import { ModalNewBase } from '../modal-new-base';

@Component({
  selector: 'app-toaster',
  templateUrl: './toaster.component.html',
  styleUrls: ['./toaster.component.sass']
})
export class ToasterComponent extends ModalNewBase implements OnInit, OnDestroy {
  isShowMsg: boolean;
  @Input() props!: {
    title: string;
    msg: string;
  };

  constructor(
    modalNew: ModalNewService,
  ) {
    super(modalNew)
    this.isShowMsg = false;
  }
  ngOnInit(): void {
    this.isShowMsg = this.props.msg !== '';

    const timeout = setTimeout(() => {
      this.isShowMsg = false;
      // this.close();
      if(timeout) {
        clearTimeout(timeout)
      }
    }, 2000)
  }

  ngOnDestroy(): void {
    this.isShowMsg = false
  }

}
