import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MemoModalComponent } from './memo-modal.component';

describe('MemoModalComponent', () => {
  let component: MemoModalComponent;
  let fixture: ComponentFixture<MemoModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MemoModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MemoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
