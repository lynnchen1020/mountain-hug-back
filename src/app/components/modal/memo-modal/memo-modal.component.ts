import { Component, Input } from '@angular/core';
import { ModalNewService } from 'src/app/services/modal-new.service';
import { ModalNewBase } from '../modal-new-base';

@Component({
  selector: 'app-memo-modal',
  templateUrl: './memo-modal.component.html',
  styleUrls: ['./memo-modal.component.sass']
})
export class MemoModalComponent extends ModalNewBase {
  @Input() props!: {
    title: string;
    msg: string;
  };

  constructor(
    modalNew: ModalNewService,
  ) {
    super(modalNew)
  }

}
