import { Component, Input, OnDestroy } from '@angular/core';
import { ModalNewService } from 'src/app/services/modal-new.service';
import { OrderService } from 'src/app/services/order.service';
import { ConfirmBoxComponent } from '../confirm-box/confirm-box.component';
import { ModalNewBase } from '../modal-new-base';
import { ToasterComponent } from '../toaster/toaster.component';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.sass']
})
export class OrderDetailsComponent extends ModalNewBase implements OnDestroy {
  modal: any = null;
  modal_e: any;
  editEmail = false;
  new_email = '';
  @Input() props!: {
    title: string;
    msg: string;
    order: any
  };
  isSubmitted = false;

  constructor(
    private order: OrderService,
    modalNew: ModalNewService,
  ) {
    super(modalNew)
  }

  ngOnDestroy(): void {
    this.isSubmitted = false;
  }

  clickSubmit() {
    this.isSubmitted = true;
  }

  public createdTimeToString(date: string) {
    return new Date(date).toLocaleString()
  }

  copyText(copied: string): void {
    navigator.clipboard.writeText(copied).then(() => {
      if(this.modal) {
        this.modal.close();
      }
      this.modal = this.modalNew.open(ToasterComponent, {
        msg: `已複製 ${copied}`
      })
    }).catch(() => {
      console.error("Unable to copy text");
    });
  }

  updateOrder(item: any) {
    this.modal_e = this.modalNew.open(ConfirmBoxComponent, {
      title: "確認修改?",
      msg: `【修改信箱並寄出】: ${this.new_email}`,
      order_id: item.order_id,
      order_name: item.name
    })

    const modalSub = this.modal_e.onSubmit.subscribe(() => {
      var payload = Object.assign(item, {
        email: this.new_email
      })

      const sub = this.order.updateOrder(payload).subscribe((res) => {
        if(res.is_success) {
          // resend mail to customers
          const sub_sub = this.order.reSendCustomerEmail(payload).subscribe(res => {
            if(sub_sub) {
              sub_sub.unsubscribe()
            }
          })
          this.modal_e.close()
        }
        this.modal_e.close()
        if(sub) {
          sub.unsubscribe()
        }
      });

      if(modalSub) {
        modalSub.unsubscribe()
      }

    })
  }
}
