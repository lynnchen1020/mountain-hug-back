import { Component, Input, OnDestroy } from '@angular/core';
import { ModalNewService } from 'src/app/services/modal-new.service';
import { ModalNewBase } from '../modal-new-base';
import { ToasterComponent } from '../toaster/toaster.component';

@Component({
  selector: 'app-confirm-box',
  templateUrl: './confirm-box.component.html',
  styleUrls: ['./confirm-box.component.sass']
})
export class ConfirmBoxComponent extends ModalNewBase implements OnDestroy {
  modal: any = null;

  @Input() props!: {
    title: string;
    msg: string;
    order_id: string;
    order_name: string;
  };
  isSubmitted = false;

  constructor(
    modalNew: ModalNewService,
  ) {
    super(modalNew)
  }

  ngOnDestroy(): void {
    this.isSubmitted = false;
  }

  clickSubmit() {
    this.isSubmitted = true;
    const isPrepaid = this.props.msg.includes('通知信')

    if(this.modal) {
      this.modal.close();
    }
    this.modal = this.modalNew.open(ToasterComponent, {
      msg: isPrepaid ? '已確認，並寄出通知信' : '已送出'
    })
  }

}
