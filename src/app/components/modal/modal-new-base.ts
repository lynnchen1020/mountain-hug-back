import { Injectable, Input, ViewRef } from '@angular/core';
import { Subject } from 'rxjs';
import { ModalNewService } from 'src/app/services/modal-new.service';

@Injectable()
export class ModalNewBase {
  @Input()
  hostView!: ViewRef;

  public onClose: Subject<void> = new Subject<void>();
  public onCancel: Subject<void> = new Subject<void>();
  public onSubmit: Subject<any> = new Subject<any>();

  constructor(protected modalNew: ModalNewService) {
  }

  public submit(data?: any): void {
    this.onSubmit.next(data);
  }

  public cancel(): void {
    this.onCancel.next();
    this.modalNew.closeByHostView(this.hostView);
  }

  public close(): void {
    this.modalNew.closeByHostView(this.hostView);
    this.onClose.next();
  }
}
