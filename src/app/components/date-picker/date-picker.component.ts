import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { getNumber, getString } from 'src/app/helper/lodash';
import { TimeService } from 'src/app/services/time.service';

interface DatePickerItem {
  date: string;
  disable: boolean;
  timestamp: string;
}

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.sass']
})
export class DatePickerComponent implements OnInit {
  @Output() dateChanged = new EventEmitter<string>();
  @Output() checkIsHoliday = new EventEmitter<object>();
  @Output() onShowPicker = new EventEmitter<boolean>();
  @Output() onHandleDisplayMonth = new EventEmitter<any>();
  // @Input() bookedDate: {
  //   [key: string]: number[]
  // } = {};
  @Input() holidays: any[] = [];
  @Input() firstHolidayPos: number = 0;
  // @Input() isTypeStart: boolean;
  // @Input() isOrder?: boolean;

  isBooked: {
    [key: string]: boolean
  } = {}

  // 1031開發起始
  // 當天已被預訂的帳數
  // 再傳進來之前算好各個天數被預訂的總帳數
  @Input() allBookedTents: {
    [key: string]: number
  } = {}
  // allBookedTents = {
  //   '1670342400000': 1, // 12/7
  //   '1670428800000': 2  // 12/8
  // }
  // 總帳數，之後再另外從 api 取得各區域總帳數
  // totalTents = 10
  @Input() totalTents: number = 0;
  arr: number[] = [];
  currentDisplayMonth: any
  currentDisplayDate: Date | any;
  // 1031開發結束

  // 日期格式2020-05-22
  private _value = '';
  @Input()
  set value(input_data: string) {
    this._value = this.parseDateString(input_data);
  }
  get value(): string {
    return this._value;
  }

  // 日期格式2020-05-22
  private _minDate = '';
  @Input()
  set minDate(input_data: string) {
    this._minDate = this.parseDateString(input_data);
  }
  get minDate(): string {
    return this._minDate;
  }

  // 日期格式2020-05-22
  private _maxDate = '';
  @Input()
  set maxDate(input_data: string) {
    this._maxDate = this.parseDateString(input_data);
  }
  get maxDate(): string {
    return this._maxDate;
  }

  showValue = '';
  isShowPicker = true;
  pickerDays: DatePickerItem[] = [];
  dayTS: number;
  displayCurrentMonth: any = 0;

  get year(): number {
    return getNumber(this.showValue.split('/'), '0');
  }

  get month(): number {
    return getNumber(this.showValue.split('/'), '1');
  }

  get day(): number {
    const time = getString(this.showValue.split(' '), '0');
    return getNumber(time.split('/'), '2');
  }

  constructor(public timeService: TimeService, private router: Router, private route: ActivatedRoute) {
    // this.isTypeStart = false;
    this.dayTS = 86400000;
  }

  ngOnInit() {
    this.showDatePicker();
    this.showValue = this.value;
    this.pickerDays = this.getPickerDays(this.showValue);
  }

  getBookedTents(area: any) {
    return
  }
  disabledBeforeToday(ts: number) {
    return new Date(this.minDate).getTime() > ts;
  }

  showDatePicker() {
    this.isShowPicker = true;
    this.showValue = this.value;
    this.pickerDays = this.getPickerDays(this.showValue);
  }

  hideDatePicker() {
    this.isShowPicker = false;
  }

  showValueAndHandleMonth(startHolidayTS: number, month: number) {
    if(!startHolidayTS) {
      return
    }
    this.showValue = new Date(startHolidayTS).toLocaleDateString();
    // const mon = new Date(this.showValue).getMonth()

    // if(this.isDisableMonth(mon)) {
    //   this.showValue = this.value;
    //   // this.handleMonth(month)
    //   console.log('休假日超過可選擇之月份')
    //   this.onHandleDisplayMonth.emit('休假日超過可選擇之月份')
    // } else {
    //   console.log('handleeeeeee')
    // }
    this.handleMonth(month)
  }

  handleMonth(count: number) {
    if (this.isDisableMonth(count)) {
      return;
    }
    const today = new Date()
    const todayLocaleString = new Date().toLocaleDateString()
    const todayTS = new Date(todayLocaleString).getTime()
    const lastEnabledDateTS = today.getTime() + 86400000 * 365

    if(new Date(this.showValue).getTime() > lastEnabledDateTS || new Date(this.showValue).getTime() < todayTS) {
      console.log('超過了啦')
      this.showValue = this.value;
      this.handleMonth(0)
      this.onHandleDisplayMonth.emit('休假日超過可選擇之月份')
      return
    }

    const d = new Date(this.showValue);
    const currentMonth = d.getMonth();
    d.setDate(20);
    d.setMonth(currentMonth + count);
    const newYear = d.getFullYear();
    const newMonth = d.getMonth() + 1 < 10 ? `${d.getMonth() + 1}` : `${d.getMonth() + 1}`;
    const newLastDate = this.getLastDateInThisMonth(`${newYear}/${newMonth}/01`).getDate();
    const selectedDate = Number(this.value.split('/')[2]);
    const newDate = newLastDate < selectedDate ? newLastDate : selectedDate;
    const newDateParse = newDate < 10 ? `${newDate}` : `${newDate}`;
    this.showValue = `${newYear}/${newMonth}/${newDateParse}`;
    this.pickerDays = this.getPickerDays(this.showValue);
  }

  stopPropagation(event: { preventDefault: () => void; returnValue: boolean; }) {
    if (event.preventDefault) {
      event.preventDefault();
    }
    if (event.returnValue) {
      event.returnValue = false;
    }
  }

  passCurrentTS(ts: number) {
    if (-new Date().getTimezoneOffset() / 60 !== 8) {
      alert(
        '系統偵測您的ip位址位於國外，暫時無法操作'
      );
      return;
    }

    const isHoliday = this.holidays.find(holiday => {
      return holiday.holiday_ts === ts;
    })

    this.checkIsHoliday.emit({
      isHoliday,
      ts,
      alias: new Date(ts).toLocaleDateString()
    })
  }

  changeDay(date: DatePickerItem) {
    if (this.allBookedTents[date.timestamp] === 10) {
      return;
    }
    const month = this.month < 10 ? `${this.month}` : `${this.month}`;
    const day = Number(date.date) < 10 ? `${date.date}` : `${date.date}`;

    this.dateChanged.emit(`${this.year}/${month}/${day}`);
    // calendar顯示開關
    this.isShowPicker = false;
    this.onShowPicker.emit(this.isShowPicker);

    // TypeStart
    // if(this.isTypeStart) {
    //   // this.dateChanged.emit(`${this.year}-${month}-${day}`);
    //   this.router.navigate([`/start-date/${this.route.snapshot.paramMap.get('id')}/order`], {
    //     queryParams: {
    //       startDate: `${this.year}-${month}-${day}`
    //     }
    //   })
    // } else {
    //   this.hideDatePicker();
    //   this.dateChanged.emit(`${this.year}-${month}-${day}`);
    // }
  }

  getPickerDays(date: string): DatePickerItem[] {
    const days: DatePickerItem[] = [];
    const lastDay = this.getLastDateInThisMonth(date);
    this.fillEmptyDays(lastDay.getDate(), lastDay.getDay(), days);
    this.setDaysFromThisMonth(lastDay.getDate(), days);
    return days;
  }

  isDisableMonth(count: number) {
    const d = new Date(this.showValue);
    const currentMonth = d.getMonth();
    d.setMonth(currentMonth + count);
    const newYear = d.getFullYear();
    const newMonth = d.getMonth() + 1 < 10 ? `${d.getMonth() + 1}` : `${d.getMonth() + 1}`;

    const clickedMonth = {
      year: Number(newYear),
      month: Number(newMonth)
    };
    const minYear = Number(this.minDate.split('/')[0]);
    // const maxYear = Number(this.maxDate.split('/')[0]);
    const maxYear = minYear + 1;
    const minMonth = Number(this.minDate.split('/')[1]);
    const maxMonth = Number(this.maxDate.split('/')[1]);

    if (count < 0) {
      if (this.minDate) {
        if (clickedMonth.year < minYear) {
          return true;
        }
        if (clickedMonth.year === minYear && clickedMonth.month < minMonth) {
          return true;
        }
      }
    }

    if (count > 0) {
      if (this.maxDate) {
        if (clickedMonth.year > maxYear) {
          return true;
        }
        if (clickedMonth.year === maxYear && clickedMonth.month > maxMonth) {
          return true;
        }
      }
    }
    return false;
  }

  isActive(date: string): boolean {
    if (date === this.day.toString() && this.isCurrentMonth() && this.isCurrentYear()) {
      return true;
    }
    return false;
  }

  // checkFull(dateTS: number, isOrder: boolean) {
  //   if(isOrder) {
  //     const minBookedDate = Math.min(...this.bookedDate);
  //     if(dateTS > minBookedDate) {
  //       return true
  //     }
  //   }

  //   return false;
  // }

  public getMinDateTS(): number {
    return new Date(this.minDate).getTime() - this.dayTS;
  }

  private isCurrentMonth() {
    const currentMonth = new Date(this.value).getMonth();
    const showMonth = new Date(this.showValue).getMonth();
    return currentMonth === showMonth;
  }

  private isCurrentYear(){
    const currentYear = new Date(this.value).getFullYear();
    const showYear = new Date(this.showValue).getFullYear();
    return currentYear === showYear;
  }

  private setDaysFromThisMonth(dateCount: number, days: DatePickerItem[]) {
    for (let i = 1; i <= dateCount; i++) {
      days.push({
        date: String(i),
        disable: this.isDisableItem(i),
        timestamp: this.getTS(this.year, this.month - 1, i)
      });
    }
  }

  private getTS(year: number, month: number, day: number): string {
    const localOffset = new Date().getTimezoneOffset() / 60;

    const diff = (-localOffset * 60 * 60 * 1000) - (8 * 60 * 60 * 1000);
    console.log('diff', diff)
    return String(new Date(year, month, day).getTime() + diff);

    // return String(new Date(year, month, day).getTime());
  }

  private fillEmptyDays(total: number, count: number, days: DatePickerItem[]) {
    let emptyDays = count - (total % 7) + 1;

    if (emptyDays < 0) {
      emptyDays = 7 + emptyDays;
    }

    for (let i = 0; i < emptyDays; i++) {
      days.push({
        date: '',
        disable: false,
        timestamp: ''
      });
    }
  }

  private getLastDateInThisMonth(date: string) {
    const separateDay = date.split('/');
    const newDate = `${separateDay[0]}/${separateDay[1]}/20`;
    const time = new Date(newDate);
    time.setMonth(time.getMonth() + 1);
    time.setDate(0);
    return time;
  }

  private isDisableItem(date: number) {
    if (this.isOverMax(date) || this.isOverMin(date)) {
      return true;
    }
    return false;
  }

  private isOverMin(date: number) {
    const min = new Date(this.minDate);

    if (this.minDate === '') {
      return false;
    }
    if (min.getFullYear() !== this.year) {
      return false;
    }
    if ((min.getMonth() + 1) !== this.month) {
      return false;
    }
    return date < min.getDate();
  }

  private isOverMax(date: number) {
    const min = new Date(this.maxDate);

    if (this.maxDate === '') {
      return false;
    }
    if (min.getFullYear() !== this.year) {
      return false;
    }
    if ((min.getMonth() + 1) !== this.month) {
      return false;
    }
    return date > min.getDate();
  }

  isCorrectFormat(input_data: string): boolean {
    // const reg = /^[1-9]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/;
    const reg = /^[1-9]\d{3}\/([1-9]|1[0-2])\/(0[1-9]|[1-2][0-9]|3[0-1])$/;
    return new RegExp(reg).test(input_data);
  }

  isCorrectDate(input_data: string): boolean {
    const time = getString(input_data.split(' '), '0');
    const date = getNumber(time.split('/'), '2');
    return new Date(input_data).getDate() === date;
  }

  parseDateString(dateString: string): string {
    if (
      !this.isCorrectFormat(dateString) ||
      !this.isCorrectDate(dateString)) {
      const newDate = this.timeService.dateTime.toString().slice(0, 10)
      const y = newDate.split('-')[0]
      const m = parseInt(newDate.split('-')[1].toString(), 10).toString()
      const d = parseInt(newDate.split('-')[2].toString().slice(0, 10), 10).toString()
      const localeDateString = `${y}/${m}/${d}`
      return localeDateString
    }
    return dateString;
  }

  // custom function
  // public isBookedAvailable(ts: number): boolean {
  //   const isBooked = {
  //     'tent_homestay': this.bookedDate['tent_homestay']?.includes(ts),
  //     'tent_a1_homestay': this.bookedDate['tent_a1_homestay']?.includes(ts),
  //     'homestay': this.bookedDate['homestay']?.includes(ts),
  //     'tent_A': this.bookedDate['tent_A']?.includes(ts),
  //     'tent_A1': this.bookedDate['tent_A1']?.includes(ts),
  //     'tent_A2': this.bookedDate['tent_A2']?.includes(ts),
  //   }
  //   return !isBooked['tent_homestay'] &&
  //   (!isBooked['tent_a1_homestay'] || !isBooked['tent_A2']) &&
  //   (!isBooked['tent_a1_homestay'] || (!isBooked['tent_A2'] && !(new Date(ts).getDay() === 5 || new Date(ts).getDay() === 6 || new Date(ts).getDay() === 0))) &&
  //   (!isBooked['homestay'] || !isBooked['tent_A']) &&
  //   (((!isBooked['tent_A1'] || !isBooked['tent_A2']) || !isBooked['homestay'] || !isBooked['tent_a1_homestay']) && (new Date(ts).getDay() !== 5 || new Date(ts).getDay() !== 6))
  // }

  // custom 公休日星期二，未來可以新增特殊休假日(只有顯示用)
  isDayOff(ts: number) {
    return new Date(ts).getDay() === 2 || new Date(ts).getDay() === 1;
  }

  isPrivateRoom(ts: number) {
    const curTS = new Date().getTime();
    return (curTS + this.dayTS * 30) < ts
  }

  // 手動休假日
  isHoliday(ts: number) {
    return this.holidays.includes(ts)
  }

  // 找出休假日
  findHoliday(ts: number) {
    return this.holidays.find((holiday: any) => {
      return holiday.holiday_ts === ts;
    })
  }
}
