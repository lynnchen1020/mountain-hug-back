import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { OrderType } from 'src/app/enum/order';
import { OrderService } from 'src/app/services/order.service';
import { } from '@fortawesome/free-solid-svg-icons';



@Component({
  selector: 'app-memo',
  templateUrl: './memo.component.html',
  styleUrls: ['./memo.component.sass']
})
export class MemoComponent implements OnInit, OnDestroy {
  @Input() type!: string;
  @Input() value?: any;
  @Output() onclose = new EventEmitter();
  @Output() onupdate = new EventEmitter();
  OrderType = OrderType;
  currentValue = '';
  isSumitted = false;

  constructor(private order: OrderService) { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.isSumitted = false;
  }

  cancel() {
    this.onclose.emit(false);
  }

  submit() {
    this.isSumitted = true;

    if(this.type === OrderType.CAMP_INN) {
      const payload = {
        order_id: this.value.order_id,
        updateInput: this.currentValue,
        currentColumnIndex: this.value.currentColumnIndex
      }
      const sub = this.order.updateCampInnOrder(payload).subscribe(() => {
        this.onupdate.emit(OrderType.CAMP_INN)
        this.isSumitted = true;
        sub.unsubscribe();
      });

      return;
    }

    if(this.type === OrderType.SMALL_CAMP_INN) {
      const payload = {
        order_id: this.value.order_id,
        updateInput: this.currentValue,
        currentColumnIndex: this.value.currentColumnIndex
      }

      const sub = this.order.updateSmallCampInnOrder(payload).subscribe(() => {
        this.onupdate.emit(OrderType.SMALL_CAMP_INN)
        sub.unsubscribe();
      });

      return;
    }

    if(this.type === OrderType.CAMP) {
      const payload = {
        order_id: this.value.order_id,
        updateInput: this.currentValue,
        currentColumnIndex: this.value.currentColumnIndex
      }
      const sub = this.order.updateCampOrder(payload).subscribe(() => {
        this.onupdate.emit(OrderType.CAMP)
        sub.unsubscribe();
      });

      return;
    }

    if(this.type === OrderType.INN) {
      const payload = {
        order_id: this.value.order_id,
        updateInput: this.currentValue,
        currentColumnIndex: this.value.currentColumnIndex
      }
      const sub = this.order.updateInnOrder(payload).subscribe(() => {
        this.onupdate.emit(OrderType.INN)
        sub.unsubscribe();
      });

      return;
    }
  }

}
