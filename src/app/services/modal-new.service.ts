
import { Injectable, Type, ViewRef } from '@angular/core';
import { Subject } from 'rxjs';
import { IModalNewService } from '../models/IModalNew-service';

@Injectable({
  providedIn: 'root'
})
export class ModalNewService implements IModalNewService {

  public modalList: any[] = [];

  public onComponentCreate: Subject<any> = new Subject<any>();
  public onComponentCloseLast: Subject<any> = new Subject<any>();
  public onComponentCloseByHostView: Subject<ViewRef> = new Subject<ViewRef>();
  public onComponentClearAll: Subject<any> = new Subject<any>();

  constructor() {}

  public get hasActiveModal(): boolean {
    return this.modalList.length > 0;
  }

  public open(component: Type<any>, props: any): any {
    this.onComponentCreate.next({ component, props });

    return this.modalList[this.modalList.length - 1];
  }

  public closeLast(): void {
    this.onComponentCloseLast.next(true);
  }

  public clearAll(): void {
    this.onComponentClearAll.next(true);
  }

  public closeByHostView(hostView: ViewRef): void {
    this.onComponentCloseByHostView.next(hostView);
  }
}
