import { from, map, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

interface IUpdatePayload {
  order_id: string,
  currentColumnIndex: number,
  updateInput: string | boolean
}

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  header = {};
  campInnApiUrl = 'https://script.google.com/macros/s/AKfycby1RIgwB_vD2lsX5F6lRNyLHoEk7_3AjrDrzsh8A6LoiXHmfX6VMg4WMa-_UoM_OkKMIQ/exec'
  smallCampInnApiUrl = 'https://script.google.com/macros/s/AKfycbxVqtQIv5eUUz2MYXbHvsmc_McP0wW1aM-p8d_kMNmbsAFFmY6Ajs2QdxiroAIeJlLXsw/exec'
  campApiUrl = 'https://script.google.com/macros/s/AKfycbwyjOY4YOGOEQ2_NCjhzb8ADACEqO3-MW3fC2hXGdtE5-OTMc5H8Pa8vvmaRm_lPEWATQ/exec'
  innApiUrl = 'https://script.google.com/macros/s/AKfycbxDXsRWETauyIb7CKPpY7uEzDXXUbgbjjGVgX_AdcZp2HTkoRzicuR_-3IbkO1OkM0Z/exec'

  constructor(private http: HttpClient) {
    // this.header = this.handleHttpHeader([
    //   { key: 'Content-Type', value: 'application/json' },
    //   { key: 'X-Requested-With', value: 'xmlhttprequest' },
    // ]);
  }

  // NOTE: 陣列產生 headers 設定
  // private handleHttpHeader(list: Array<Header>): HttpHeaders {
  //   const res: any = {};
  //   list.forEach((item) => {
  //     res[item.key] = item.value;
  //   });

  //   return new HttpHeaders({ res });
  // }

  public ping(): Observable<any> {
    return new Observable(subscriber => {
      this.http.get(`${environment.apiUrl}/api/ping`, {responseType: 'text'}).pipe(
        map(res => res)
      ).subscribe(data => {
        subscriber.next(data);
        subscriber.complete();
      }),
      (err: any) => {
        subscriber.next(err);
        subscriber.complete();
      }
    })
  }

  public getCampOrders(): Observable<any> {
    return new Observable(subscriber => {
      this.http.get(this.campApiUrl).pipe(
        map(res => res)
      ).subscribe(data => {
        subscriber.next(data);
        subscriber.complete();
      }),
      (err: any) => {
        subscriber.next(err);
        subscriber.complete();
      }
    })
  }

  public getSmallCampInnOrders(): Observable<any> {
    return new Observable(subscriber => {
      this.http.get(this.smallCampInnApiUrl).pipe(
        map(res => res)
      ).subscribe(data => {
        subscriber.next(data);
        subscriber.complete();
      }),
      (err: any) => {
        subscriber.next(err);
        subscriber.complete();
      }
    })
  }

  public getInnOrders(): Observable<any> {
    return new Observable(subscriber => {
      this.http.get(this.innApiUrl).pipe(
        map(res => res)
      ).subscribe(data => {
        subscriber.next(data);
        subscriber.complete();
      }),
      (err: any) => {
        subscriber.next(err);
        subscriber.complete();
      }
    })
  }
  public getCampInnOrders(): Observable<any> {
    return new Observable(subscriber => {
      this.http.get(this.campInnApiUrl).pipe(
        map(res => res)
      ).subscribe(data => {
        subscriber.next(data);
        subscriber.complete();
      }),
      (err: any) => {
        subscriber.next(err);
        subscriber.complete();
      }
    })
  }

  public updateCampOrder(payload: IUpdatePayload): Observable<any> {
    // const payload = {
    //   order_id: 'i_1657388387166',
    //   curColumnIndex: 5, //需要改的欄位
    //   updateInput: false,
    // }
    return this.http.post(this.campApiUrl, JSON.stringify(payload));
  }

  public updateInnOrder(payload: IUpdatePayload): Observable<any> {
    // const payload = {
    //   order_id: 'i_1657388387166',
    //   curColumnIndex: 5, //需要改的欄位
    //   updateInput: false,
    // }

    return this.http.post(this.innApiUrl, JSON.stringify(payload));
  }

  public updateCampInnOrder(payload: IUpdatePayload): Observable<any> {
    // const payload = {
    //   order_id: 'i_1657388387166',
    //   curColumnIndex: 5, //需要改的欄位
    //   updateInput: false,
    // }

    return this.http.post(this.campInnApiUrl, JSON.stringify(payload));
  }

  public updateSmallCampInnOrder(payload: IUpdatePayload): Observable<any> {
    // const payload = {
    //   order_id: 'i_1657388387166',
    //   curColumnIndex: 5, //需要改的欄位
    //   updateInput: false,
    // }
    return this.http.post(this.smallCampInnApiUrl, JSON.stringify(payload));
  }

  // New API start
  public getOrders(payload: string): Observable<any> {
    const url = `${environment.apiUrl}/api/back/order/${payload}`
    return this.http.get(url, {responseType: 'json'})
  }

  public updateOrder(payload: any): Observable<any> {
    const url = `${environment.apiUrl}/api/back/order/${payload.area}/${payload.order_id}`
    return this.http.patch(url, payload)
  }

  public sendPrepaidEmail(payload: any){
    const url = `${environment.apiUrl}/api/mailsend_prepaid`
    return this.http.post(url, payload);
  }

  public reSendCustomerEmail(payload: any){
    const url = `${environment.apiUrl}/api/resend_mail`
    return this.http.post(url, payload);
  }

  public deleteEvent(payload: any): Observable<any> {
    const url = `${environment.apiUrl}/api/calendar`
    return this.http.post(url, payload);
  }

  public sendHoliday(payload: any): Observable<any> {
    const url = `${environment.apiUrl}/api/back/holiday`
    return new Observable(subscriber => {
      this.http.post(url, payload).pipe(
        map(res => res)
      ).subscribe(data => {
        subscriber.next(data);
        subscriber.complete();
      }),
      (err: any) => {
        subscriber.next(err);
        subscriber.complete();
      }
    })
  }

  public getHoliday(): Observable<any> {
    const url = `${environment.apiUrl}/api/back/holiday`
    return this.http.get(url, {responseType: 'json'})
  }

  public deleteHoliday(payload: any): Observable<any> {
    const url = `${environment.apiUrl}/api/back/holiday/delete`
    return this.http.post(url, payload)
  }

  // 登記宿屋 Api

  public sendOrder(payload: any): Observable<any> {
    const url = `${environment.apiUrl}/api/order/${payload.area}`
    return new Observable(subscriber => {
      this.sendEmail(payload).subscribe((res: any) => {
        const obj = Object.assign(payload, {
          event_id: res.event_id
        });

        this.http.post(url, obj).subscribe((res) => {
          if(res) {
            subscriber.next(true);
            subscriber.complete();
          }
        })
      });
    })
  }

  public sendEmail(payload: any){
    const url = `${environment.apiUrl}/api/mailsend`
    return this.http.post(url, payload);
  }
  // New API end
}
