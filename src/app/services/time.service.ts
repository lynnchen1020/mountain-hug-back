import { Injectable } from '@angular/core';
import { interval, Subject, Observable, Subscription } from 'rxjs';

import { DateTime } from '../classes/dateTime';
import { ItimeService } from '../models/itime-service';
// import { InitConfigService } from './init-config.service';
// import { ENVNAME } from 'enum/env-name';
// import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TimeService implements ItimeService {
  dateTime: DateTime;

  tick$: Subject<number>;

  subscription!: Subscription;
  oldUpdateTime = 0;
  constructor() {
    this.dateTime = new DateTime();
    this.tick$ = new Subject<number>();
  }

  initialize(): Observable<boolean> {
    return new Observable(subscribe => {
      // this.dateTime.setTimezoneOffset(this.initConfiService.data.InitTimezoneOffset);
      // this.dateTime.setTime(this.initConfiService.data.InitServerTime * 1000);
      // if (!this.subscription && environment.name !== ENVNAME.DEVTEST) {
      //   this.runTimer();
      // }
      subscribe.next(true);
      subscribe.complete();
    });
  }

  private runTimer() {
    this.subscription = interval(1000).subscribe(() => {
      let nextTick = 0;
      if(this.oldUpdateTime === 0){
        nextTick = this.dateTime.getTime() + 1000;
        this.oldUpdateTime = new Date().getTime();
      } else {
        const currentUpdateTime = new Date().getTime();
        nextTick = this.dateTime.getTime() + ( currentUpdateTime - this.oldUpdateTime) ;
        this.oldUpdateTime = new Date().getTime();
      }
      this.dateTime.setTime(nextTick);

      this.tick$.next(nextTick);
    });
  }
}
