import { OrderTypeTentsMax } from "../enum/order";

export function padLeft(str: string | number, length: number): string {
  const s = '' + str;
  return s.length >= length ? s : padLeft('0' + str, length);
}

export function addZero(n: string): string {
  const result = Number(n);
  if (result < 10) {
    return `0${n}`;
  }

  return `${n}`;
}

interface IDayMapping {
  [key: string]: string
}

export function getCurrentDay(dateTime: string): string {
  const dayMapping: IDayMapping = {
    "0": '日',
    "1": '一',
    "2": '二',
    "3": '三',
    "4": '四',
    "5": '五',
    "6": '六',
  }

  return dayMapping[String(new Date(dateTime).getDay())];
}

export function getOfficialHolidays() {
  const official_holidays = {
    year_2022: ['2022/10/9', '2022/10/10'],
    year_2023:[
      '2023/1/1',
      '2023/1/2',
      '2023/1/2',
      '2023/1/21',
      '2023/1/22',
      '2023/1/23',
      '2023/1/24',
      '2023/2/26',
      '2023/2/27',
      '2023/2/28',
      '2023/4/2',
      '2023/4/3',
      '2023/4/4',
      '2023/4/5',
      '2023/4/30',
      '2023/5/1',
      '2023/6/22',
      '2023/6/23',
      '2023/10/8',
      '2023/10/9',
      '2023/10/10'
    ]
  }

  return official_holidays;
}

// 日期格式轉換 timestamp -> yyyy/mm/dd
export function timestampToSlashDate(ts: number) {
  return new Date(ts).toLocaleDateString();
}

// 露營區專用 - 取得各營區帳數上限值
export function getAreaTentsMax(area: string) {
  switch(area) {
    case 'camp_a':
      return OrderTypeTentsMax.CAMP_A
    case 'camp_b':
      return OrderTypeTentsMax.CAMP_B
    case 'camp_c':
      return OrderTypeTentsMax.CAMP_C
    case 'camp_d':
      return OrderTypeTentsMax.CAMP_D
    case 'inn':
      return OrderTypeTentsMax.INN
    default:
      return ""
  }
}
