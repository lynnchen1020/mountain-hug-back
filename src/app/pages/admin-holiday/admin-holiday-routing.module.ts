import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminHolidayComponent } from './admin-holiday.component';

const routes: Routes = [
  {
    path: '',
    component: AdminHolidayComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminHolidayRoutingModule { }
