import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminHolidayRoutingModule } from './admin-holiday-routing.module';
import { AdminHolidayComponent } from './admin-holiday.component';
import { ShareModule } from 'src/app/module/share/share.module';


@NgModule({
  declarations: [AdminHolidayComponent],
  imports: [
    CommonModule,
    ShareModule,
    AdminHolidayRoutingModule
  ]
})
export class AdminHolidayModule { }
