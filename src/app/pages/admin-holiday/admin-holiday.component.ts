import { Component, Inject, OnInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { OrderService } from 'src/app/services/order.service';
import { intersection } from 'src/app/helper/lodash';
import { ModalNewService } from 'src/app/services/modal-new.service';
import { pullAll } from 'lodash';
import { DefaultConfirmBoxComponent } from 'src/app/components/modal/default-confirm-box/default-confirm-box.component';
import { getCurrentDay } from 'src/app/helper/helper';

@Component({
  selector: 'app-admin-holiday',
  templateUrl: './admin-holiday.component.html',
  styleUrls: ['./admin-holiday.component.sass']
})
export class AdminHolidayComponent implements OnInit {
  dateMin = '';
  dateMax = '';
  dateStart = '';
  startHolidayMonth = 0;
  startHolidayTS = 0;
  displayMonth = 0;
  displayMonthTS = 0;
  getCurrentDay = getCurrentDay;

  message: {[key: string]: string} = {}
  holidays: any[] = [];

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private modal: ModalNewService,
    private order: OrderService) { }

  ngOnInit(): void {
    this.getHolidays();
  }

  getHolidays() {
    const sub = this.order.getHoliday().subscribe(res => {
      if(res) {
        this.holidays = res;
      }
      if(sub) {
        sub.unsubscribe();
      }
    })
  }

  send() {
    if (-new Date().getTimezoneOffset() / 60 !== 8) {
      alert(
        '系統偵測您的ip位址位於國外，暫時無法操作'
      );
      return;
    }

    const d = new Date().toLocaleDateString()
    const d_ts = new Date(d).getTime();
    const start_timestamp = <HTMLInputElement>this.document.querySelector('.start-date');
    const end_timestamp = <HTMLInputElement>this.document.querySelector('.end-date');

    const start_ts = new Date(new Date(start_timestamp.valueAsNumber).toDateString()).getTime()
    const end_ts = new Date(new Date(end_timestamp.valueAsNumber).toDateString()).getTime()

    const beforeToday = start_ts < d_ts
    const biggerThanStart = end_ts < start_ts
    const empty = !start_ts || !end_ts;

    this.startHolidayMonth = new Date(start_ts).getMonth();
    this.startHolidayTS = new Date(start_ts).getTime();

    this.message = {
      beforeToday: start_ts < d_ts ? '開始日期小於今天，請重新選取' : '',
      biggerThanStart: biggerThanStart ? '結束時間不可大於開始時間' : "",
      empty: empty ? "日期不可為空" : "",
    }
    if(biggerThanStart || empty || beforeToday) {
      return
    }

    const slice = (end_ts - start_ts) / 86400000
    let holidays: any[] = []
    switch(slice) {
      case 0: {
        holidays = [start_ts]
        break;
      }
      default:
        for(let i = 0; i <= slice; i++) {
          holidays.push(start_ts + (86400000 * i))
        }
    }

    if(holidays.length > 10) {
      this.message = {
        over10days: "休假日請勿連續選取超過10天"
      }
      return
    }

    const onlineHolidays = this.holidays.map(item => item?.holiday_ts)
    const pickedHolidays = pullAll(holidays, intersection(onlineHolidays, holidays))

    // if(intersection(onlineHolidays, holidays).length > 0) {
    //   let intersaction_dates = ''
    //   intersection(onlineHolidays, holidays).forEach((ts: number | any) => {
    //     intersaction_dates += `${new Date(ts).toLocaleDateString()},`
    //   })
    //   this.message = {
    //     intersaction_dates: `${intersaction_dates}，日期已休假了，請重新選擇`
    //   }
    //   return
    // }

    pickedHolidays.forEach((ts) => {
      const payload = {
        holiday_ts: ts,
        alias: new Date(ts).toLocaleDateString("zh-TW")
      }
      const sub = this.order.sendHoliday(payload).subscribe(res => {
        if(res.status) {
          const sub = this.order.getHoliday().subscribe((data) => {
            this.holidays = data
            if(sub) {
              sub.unsubscribe()
            }
          })
        } else {
          this.message = {
            duplicate: res.message
          }
        }
        if(sub) {
          sub.unsubscribe()
        }
      })
    })
  }

  handleDisplayMonth($event: any) {
    this.message = {
      over_enabled_month: $event
    }
    // const { currentDisplayMonth, currentDisplayTS } = $event;
    // this.displayMonth = currentDisplayMonth
    // this.displayMonthTS = currentDisplayTS
    // this.send();
  }

  deleteDate(checkHoliday: any) {
    if(!checkHoliday.isHoliday) {
      const modal_ask = this.modal.open(DefaultConfirmBoxComponent, {
        title: "是否休假",
        msg: `【 ${checkHoliday.alias} 】要休假嗎?`
      })

      modal_ask.onSubmit.subscribe(() => {
        const payload = {
          holiday_ts: checkHoliday.ts,
          alias: new Date(checkHoliday.ts).toLocaleDateString("zh-TW")
        }
        const sub = this.order.sendHoliday(payload).subscribe(res => {
          if(res.status) {
            const sub = this.order.getHoliday().subscribe((data) => {
              this.holidays = data
              if(sub) {
                sub.unsubscribe()
              }
            })
          } else {
            this.message = {
              duplicate: res.message
            }
          }
          if(sub) {
            modal_ask.close();
            sub.unsubscribe()
          }
        })
      })
      return
    }

    const modal_ask = this.modal.open(DefaultConfirmBoxComponent, {
      title: "是否取消",
      msg: `【 ${checkHoliday.alias} 】取消休假嗎?`

    })


    modal_ask.onSubmit.subscribe(() => {
      const payload = {
        holiday_ts: checkHoliday.ts
      }
      const sub = this.order.deleteHoliday(payload).subscribe(res => {
        if(res) {
          const sub = this.order.getHoliday().subscribe(data => {
            this.holidays = data
            if(sub) {
              sub.unsubscribe();
            }
          })
        }
        if(sub) {
          sub.unsubscribe()
          modal_ask.close();
        }
      })
    })


  }

  getDateString() {
    const start = new Date();
    const end = new Date();

    start.setDate((start.getDate()));

    const USStartDate = start.setHours((start.getHours() + 12));
    // const USEndDate = end.setHours((end.getHours() - 12)) + dayTS * 90; // 開放 90 days

    const USEndDate = end.setMonth((end.getMonth() + 6)) // 開放6個月
    // const USEndDateTS = +this.getLastDateInThisMonth(new Date(USEndDate).toLocaleDateString())


    this.dateMin = this.formatDate(USStartDate);
    // this.dateMax = this.formatDate(USEndDateTS);
    // this.dateStart = this.dateMin;
    // this.dateEnd = this.dateMax;

    // const leaveTS = new Date(this.dateStart).getTime() + this.dayTS
    // this.dateLeave = new Date(leaveTS).toLocaleDateString();
  }

  private formatDate(usDate: number): string {
    const newTime: Date = new Date(usDate);
    // Date 轉成 YYYY-MM-DD
    return `${newTime.getFullYear()}/${(newTime.getMonth() + 1).toString()}/${newTime.getDate().toString()}`;
  }

  changeStartDate($event: string) {
    this.dateStart = $event
    // this.isShowCards = true;
    // this.showCardsAnimation();

    // const startTS = new Date(this.changeDateToSlashFormat(this.dateStart)).getTime();

    // const leaveTS = startTS + this.dayTS * this.nightsMapping[this.currentNightsIdx].nights
    // this.dateLeave = new Date(leaveTS).toLocaleDateString();

    // this.startTS = startTS;
    // this.leaveTS = leaveTS;

    // this.orderService.dateStart = this.dateStart
    // this.orderService.dateLeave = this.dateLeave

    // this.countDayBeforePrivateRoom();
    // this.countDayBeforeHoliday();
    // this.countDayFull();

    // const el = document.getElementsByClassName('wrapper')[0]
    // if(el) {
    //   el.scrollTop = 0
    // }
  }

}
