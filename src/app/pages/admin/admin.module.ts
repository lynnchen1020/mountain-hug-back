import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { StickyDirectiveModule } from 'src/app/directives/sticky-table.directive';
import { ShareModule } from 'src/app/module/share/share.module';


@NgModule({
  declarations: [AdminComponent],
  imports: [
    CommonModule,
    StickyDirectiveModule,
    ShareModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
