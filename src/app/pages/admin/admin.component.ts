import { Component, OnInit } from '@angular/core';
import { zip } from 'rxjs';
import { OrderType } from 'src/app/enum/order';
import { OrderService } from 'src/app/services/order.service';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { ModalNewService } from 'src/app/services/modal-new.service';
import { ConfirmBoxComponent } from 'src/app/components/modal/confirm-box/confirm-box.component';
import { countBy, get, uniq } from 'src/app/helper/lodash';
import { concat, flatten } from 'lodash';
import { OrderDetailsComponent } from 'src/app/components/modal/order-details/order-details.component';
import { addZero, getAreaTentsMax, getCurrentDay } from 'src/app/helper/helper';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.sass']
})
export class AdminComponent implements OnInit {
  OrderType = OrderType;
  data: any = [];
  copyData: any = [];
  faCheck = faCheck;
  modal_c: any;
  modal_u: any;
  modal_o: any;
  modal_order: any;
  nightRushConfig: {[key: string]: string}= {};
  isCanceledOrder: {[key: string]: boolean}= {};
  isNotPrepaid: {[key: string]: boolean}= {};
  initCanceledData: any = {};
  getCurrentDay = getCurrentDay;
  holidays: any[] = [];

  constructor(
    private order: OrderService,
    private modal: ModalNewService
  ) {
    this.isCanceledOrder = {
      'camp_a': false,
      'camp_b': false,
      'camp_c': false,
      'camp_d': false,
      'inn': false
    }

    this.isNotPrepaid = {
      'camp_a': false,
      'camp_b': false,
      'camp_c': false,
      'camp_d': false,
      'inn': false
    }
  }

  ngOnInit(): void {
    // this.order.getOrders('camp_a').subscribe(data => {
    //   console.log('response', data)
    //   this.data = data;
    // })
    this.getAllOrders();
    this.getHolidays();
  }

  private sortStartDate(arr: any) {
    return arr.sort((a:any, b:any) => {
      return  new Date(a.start_date).getTime() - new Date(b.start_date).getTime()
    })
  }

  getHolidays() {
    const sub = this.order.getHoliday().subscribe(res => {
      if(res) {
        this.holidays = res;
      }
      if(sub) {
        sub.unsubscribe();
      }
    })
  }

  getAllOrders() {
    const sub = zip(
      this.order.getOrders(OrderType.CAMP_A),
      this.order.getOrders(OrderType.CAMP_B),
      this.order.getOrders(OrderType.CAMP_C),
      this.order.getOrders(OrderType.CAMP_D),
      this.order.getOrders(OrderType.INN)
    ).subscribe(res => {
      this.data = res;
      this.data[0] = this.sortStartDate(res[0])
      this.data[1] = this.sortStartDate(res[1])
      this.data[2] = this.sortStartDate(res[2])
      this.data[3] = this.sortStartDate(res[3])
      this.data[4] = this.sortStartDate(res[4])

      this.copyData[0] = this.sortStartDate(res[0])
      this.copyData[1] = this.sortStartDate(res[1])
      this.copyData[2] = this.sortStartDate(res[2])
      this.copyData[3] = this.sortStartDate(res[3])
      this.copyData[4] = this.sortStartDate(res[4])

      const data: {[key: string]: number[]} = {
        'camp_a': flatten(get(res, '0', '').filter((item: any) => item.is_canceled_order === false).map((item: any) => item.tents_per_day)),
        'camp_b': flatten(get(res, '1', '').filter((item: any) => item.is_canceled_order === false).map((item: any) => item.tents_per_day)),
        'camp_c': flatten(get(res, '2', '').filter((item: any) => item.is_canceled_order === false).map((item: any) => item.tents_per_day)),
        'camp_d': flatten(get(res, '3', '').filter((item: any) => item.is_canceled_order === false).map((item: any) => item.tents_per_day)),
        'inn': flatten(get(res, '4', '').filter((item: any) => item.is_canceled_order === false).map((item: any) => item.tents_per_day)),
      }

      // 算出各區剩餘帳數
      const eachDayLeftTents = {
        'uniqBookedDateA': uniq(flatten(get(res, '0', '').map((item: any) => item.tents_per_day))),
        'uniqBookedDateB': uniq(flatten(get(res, '1', '').map((item: any) => item.tents_per_day))),
        'uniqBookedDateC': uniq(flatten(get(res, '2', '').map((item: any) => item.tents_per_day))),
        'uniqBookedDateD': uniq(flatten(get(res, '3', '').map((item: any) => item.tents_per_day))),
        'uniqBookedDateInn': uniq(flatten(get(res, '4', '').map((item: any) => item.tents_per_day))),
      }

      // console.log('eachDayLeftTents', eachDayLeftTents)

      let a_config: any = {}
      eachDayLeftTents['uniqBookedDateA'].forEach(uniqTS => {
        a_config[String(uniqTS)] = countBy(flatten(data[OrderType.CAMP_A]), uniqTS)
      })
      let b_config: any = {}
      eachDayLeftTents['uniqBookedDateB'].forEach(uniqTS => {
        b_config[String(uniqTS)] = countBy(flatten(data[OrderType.CAMP_B]), uniqTS)
      })
      let c_config: any = {}
      eachDayLeftTents['uniqBookedDateC'].forEach(uniqTS => {
        c_config[String(uniqTS)] = countBy(flatten(data[OrderType.CAMP_C]), uniqTS)
      })
      let d_config: any = {}
      eachDayLeftTents['uniqBookedDateD'].forEach(uniqTS => {
        d_config[String(uniqTS)] = countBy(flatten(data[OrderType.CAMP_D]), uniqTS)
      })
      let inn_config: any = {}
      eachDayLeftTents['uniqBookedDateInn'].forEach(uniqTS => {
        inn_config[String(uniqTS)] = countBy(flatten(data[OrderType.INN]), uniqTS)
      })

      this.nightRushConfig = {
        'camp_a': a_config,
        'camp_b': b_config,
        'camp_c': c_config,
        'camp_d': d_config,
        'inn': inn_config,
      }

      this.getInitData();

      if(sub) {
        sub.unsubscribe();
      }
    })
  }

  getInitData() {
    this.initCanceledData = {
      'camp_a': this.data[0].filter((item: any) => {
        return !item.is_canceled_order && !this.isDue(item.end_date)
      }),
      'camp_b': this.data[1].filter((item: any) => {
        return !item.is_canceled_order && !this.isDue(item.end_date)
      }),
      'camp_c': this.data[2].filter((item: any) => {
        return !item.is_canceled_order && !this.isDue(item.end_date)
      }),
      'camp_d': this.data[3].filter((item: any) => {
        return !item.is_canceled_order && !this.isDue(item.end_date)
      }),
      'inn': this.data[4].filter((item: any) => {
        return !item.is_canceled_order && !this.isDue(item.end_date)
      }),
    }
  }

  filterCanceledData(type: string) {
    const typeMap: any = {
      'camp_a': 0,
      'camp_b': 1,
      'camp_c': 2,
      'camp_d': 3,
      'inn': 4,
    }
    this.isCanceledOrder[type] = !this.isCanceledOrder[type]
    if(this.isCanceledOrder[type]) {
      this.initCanceledData[type] = this.copyData[typeMap[type]]
      return
    }
    this.initCanceledData[type] = this.data[typeMap[type]].filter((item: any) => !item.is_canceled_order && !this.isDue(item.end_date))
  }

  updateOrder(item: any, action: string) {
    // Cancel Order
    if(action === 'cancel') {
      const trans_isChecked = !item.is_canceled_order ? '是' : '否'

      this.modal_c = this.modal.open(ConfirmBoxComponent, {
        title: "確認修改?",
        msg: `【要取消訂單嗎】: ${trans_isChecked}`,
        order_id: item.order_id,
        order_name: item.name
      })

      const modalSub = this.modal_c.onSubmit.subscribe(() => {
        const startDateSplit = item.start_date.split('/');
        const mon = addZero(startDateSplit[1])
        const date = addZero(startDateSplit[2])
        const startDateWithoutSlash = `${startDateSplit[0]}${mon}${date}`

        const f: HTMLElement | any = document.getElementById('iframe');
        if(f) {
          const updatedMonthSrc = `https://calendar.google.com/calendar/embed?height=600&wkst=1&bgcolor=%23ffffff&ctz=Asia%2FTaipei&showTitle=0&showNav=1&showPrint=0&showTabs=0&showCalendars=0&showTz=0&showDate=1&src=NDBhMDgyY2Q0MjRlYTU2OWNlMjU5NGI5MDdjZjczN2ZiMDAzZGU5NGRlOTlkZTkzZDdmN2RmNmNiMDQyYjNhNkBncm91cC5jYWxlbmRhci5nb29nbGUuY29t&src=Mzg5MDZlMDZkNWRiMTUxMTA4NWZhMjk2MjQwY2E1OTc2ZmZmODg0Mzg3YjcyMjc3NTg5NjE4ZTQzMWNlZjgwM0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t&src=NGY2MGZjNDQ1NTEyZTA3MzA0ZTg5Y2Q4YzY3YTBlYzQ4Yzk3NjNiNzFlN2MwOTY3NTI2ZWMwMDVkZmNlZDFlMEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t&src=YWEzZmJlM2NjNjNlOGEyZDRjMmMwNTRjNGM3OWNlZTRiODJkYTM4NjgyZTFiMjZiZjEzY2ZmY2MxN2NlNjM5Y0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t&src=ODEyYzM5MDgxM2MzZmQ2MzI4MDA2NGM3N2VkN2IwYzQ3MGY5MzAyZDU0NDVlNTFmYjg3Y2IxOTEyNzAyNmIwZUBncm91cC5jYWxlbmRhci5nb29nbGUuY29t&color=%23D81B60&color=%23F6BF26&color=%23009688&color=%234285F4&color=%23EF6C00&dates=${startDateWithoutSlash}/${startDateWithoutSlash}`

          f.src = updatedMonthSrc;
        }
        item.is_canceled_order = !item.is_canceled_order

        var payload = Object.assign(item, {
          is_canceled_order: item.is_canceled_order
        })
        const sub = this.order.updateOrder(payload).subscribe(() => {
          this.getAllOrders();
          const eventSub = this.order.deleteEvent(item).subscribe(() => {
            if(eventSub) {
              eventSub.unsubscribe();
            }
          })
          this.modal_c.close()
          if(sub) {
            sub.unsubscribe()
          }
        });

        if(modalSub) {
          modalSub.unsubscribe()
        }
      })

    }

    // Prepaid Fee
    if(action === 'prepaid') {
      const trans_isChecked = !item.is_prepaid ? '是' : '否'

      this.modal_u = this.modal.open(ConfirmBoxComponent, {
        title: "確認修改?",
        msg: `【客人已付訂金嗎】: ${trans_isChecked}${!item.is_prepaid ? '，同步寄出通知信' : ''}`,
        order_id: item.order_id,
        order_name: item.name
      })

      const modalSub = this.modal_u.onSubmit.subscribe(() => {
        item.is_prepaid = !item.is_prepaid

        var payload = Object.assign(item, {
          is_prepaid: item.is_prepaid
        })

        const sub = this.order.updateOrder(payload).subscribe((res) => {
          if(res.is_success && item.is_prepaid) {

            const sub_sub = this.order.sendPrepaidEmail(payload).subscribe(res => {
              // console.log('sendPrepaidEmail res', res)

              if(sub_sub) {
                sub_sub.unsubscribe()
              }
            })

            this.modal_u.close()
          }
          this.modal_u.close()
          if(sub) {
            sub.unsubscribe()
          }
        });

        if(modalSub) {
          modalSub.unsubscribe()
        }

      })
    }

    // Over Night
    if(action === 'nightgoing') {
      const trans_isChecked = !item.is_night_going ? '是' : '否'

      this.modal_o = this.modal.open(ConfirmBoxComponent, {
        title: "確認修改?",
        msg: `【要夜衝嗎】: ${trans_isChecked}`,
        order_id: item.order_id,
        order_name: item.name
      })

      const modalSub = this.modal_o.onSubmit.subscribe(() => {
        item.is_night_going = !item.is_night_going

        if(item.is_night_going) {
          for(let i = 0; i < item.tents; i++) {
            item.tents_per_day.push(item.tents_per_day[0] - 86400000)
          }

          var payload = Object.assign(item, {
            is_night_going: item.is_night_going,
            tents_per_day: item.tents_per_day.sort()
          })
        } else {

          var payload = Object.assign(item, {
            is_night_going: item.is_night_going,
            tents_per_day: item.tents_per_day.splice(item.tents, item.tents)
          })
        }

        const sub = this.order.updateOrder(payload).subscribe((res) => {
          if(res.is_success && item.is_night_going) {

            this.modal_o.close()
          }
          this.modal_o.close()
          if(sub) {
            sub.unsubscribe()
          }
        });

        if(modalSub) {
          modalSub.unsubscribe()
        }
      })
    }
  }

  // 過期的訂單
  isDue(leaveDate: string) {
    let curTS = new Date().getTime() - 86400000;
    let leaveTS = new Date(leaveDate).getTime();
    return curTS > leaveTS
  }

  // 是否可夜衝
  isNightRush(area: string, startDate: string, tents: number, isNightGoing: boolean) {
    const beforeStartDayTS = new Date(startDate).getTime() - 86400000;
    const isConflicHoliday = this.holidays.find(holiday => holiday.holiday_ts === beforeStartDayTS)
    const hasTentsBeforeStartDay = (Number(getAreaTentsMax(area)) - get(this.nightRushConfig, `${area}.${beforeStartDayTS}`, 0)) >= tents;

    return isNightGoing || hasTentsBeforeStartDay && !isConflicHoliday
  }

  public getAreaTitle(area: string) {
    switch(area) {
      case 'camp_a':
        return '五葉松 A區'
      case 'camp_b':
        return '落羽松 L區'
      case 'camp_c':
        return '雨棚區'
      case 'camp_d':
        return 'XX區'
      case 'inn':
        return '山中宿屋'
      default:
        return ""
    }
  }

  public notPaidPreFee(date: string) {
    const ts = new Date().getTime();
    const createdTS = new Date(date).getTime() + (86400000 * 2);
    return ts >= createdTS
  }

  openDetail(order: any) {
    this.modal_order = this.modal.open(OrderDetailsComponent, {
      title: "訂單詳情",
      msg: "",
      order
    })
  }

  public toggleNotPrepaid(area: string) {
    this.isNotPrepaid[area] = !this.isNotPrepaid[area]
  }

}

