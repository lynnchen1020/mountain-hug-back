import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { interval } from 'rxjs';
import { ToasterComponent } from 'src/app/components/modal/toaster/toaster.component';
import { timestampToSlashDate } from 'src/app/helper/helper';
import { ModalNewService } from 'src/app/services/modal-new.service';
import { OrderService } from 'src/app/services/order.service';

interface IDateInfo {
  start_ts: number;
  end_ts: number;
  nights: number;
  tents_per_day: number[];
}

@Component({
  selector: 'app-booking',
  templateUrl: './booking.component.html',
  styleUrls: ['./booking.component.sass'],
})
export class BookingComponent implements OnInit {
  innFee = 5800;
  name = '';
  phone = '';
  dateInfo = {
    start_ts: 0,
    end_ts: 0,
    nights: 0,
    tents_per_day: [0],
  };
  isSubmitted = true;

  constructor(
    private orderService: OrderService,
    private router: Router,
    private modalNew: ModalNewService,
    @Inject(DOCUMENT) private document: Document
  ) {}

  ngOnInit(): void {}

  getStartEndDate(): IDateInfo | any {
    let tents_per_day = [];
    const start_timestamp = <HTMLInputElement>(
      this.document.querySelector('.start-date')
    );
    const end_timestamp = <HTMLInputElement>(
      this.document.querySelector('.end-date')
    );

    const start_ts = new Date(
      new Date(start_timestamp.valueAsNumber).toDateString()
    ).getTime();
    const end_ts = new Date(
      new Date(end_timestamp.valueAsNumber).toDateString()
    ).getTime();

    const nights = (end_ts - start_ts) / 86400000;

    for (let i = 0; i < nights; i++) {
      const ts = start_ts + 86400000 * i;
      tents_per_day.push(ts);
    }

    const dateInfo = {
      start_ts,
      end_ts,
      nights,
      tents_per_day,
    };
    this.dateInfo = dateInfo;
  }

  checkValid(): boolean | any {
    return true;
  }

  public submit() {
    this.getStartEndDate();

    // 姓名電話任一為空
    if (this.name === '' || this.phone === '') {
      alert('姓名電話未填寫');
      return;
    }

    // 日期任一為空
    if (
      Number.isNaN(this.dateInfo.start_ts) ||
      Number.isNaN(this.dateInfo.end_ts)
    ) {
      alert('日期未填寫');
      return;
    }

    // 離開日期早於入住日期
    if (
      this.dateInfo.end_ts < this.dateInfo.start_ts ||
      this.dateInfo.end_ts === this.dateInfo.start_ts
    ) {
      alert('入住期間有誤，請再次檢查正確性');
      return;
    }

    const order = {
      name: this.name,
      phone: this.phone,
      email: '',
      adults: 1,
      children: 1,
      addBreakfast: 0,
      add_bed: 0,
      order_id: `inn_${String(new Date().getTime())}`,
      area: 'inn',
      start_date: timestampToSlashDate(this.dateInfo.start_ts),
      timestamp_start_date: new Date(
        timestampToSlashDate(this.dateInfo.start_ts)
      ).getTime(),
      end_date: timestampToSlashDate(this.dateInfo.end_ts),
      tents_per_day: this.dateInfo.tents_per_day,
      total_fee: this.innFee * this.dateInfo.nights, //this.totalFee().toString(),
      nights: this.dateInfo.nights, // this.orderService.getBookedNights()?.toString(),
      // [OrderTent.TENTS]: this.order.tents.toString(),
      prepaid_fee: (this.innFee * this.dateInfo.nights) / 2, // this.prePaidFee().toString(), //計算5成訂金
      rest_fee: (this.innFee * this.dateInfo.nights) / 2, // this.restFee().toString(), //計算尾款
      // [OrderTent.IS_PAID_PRE_FEE]: '否',
      // [OrderTent.IS_PAID_REST_FEE]: '否',
      tents: 1,
      is_night_going: false,
      is_prepaid: false,
      order_source: '網站訂單',
      created_time: new Date().getTime(),
    };

    if (confirm('確定送出?') === true) {
      this.isSubmitted = false;
      this.clickSubmit();
      // 送出訂單
      const sub = this.orderService.sendOrder(order).subscribe((res) => {
        if (res) {
          this.router.navigate(['/']);
          this.isSubmitted = true;
        }

        sub.unsubscribe();
      });
    } else {
      return;
    }
  }

  clickSubmit() {
    this.modalNew.open(ToasterComponent, {
      msg: '已送出',
    });
  }
}
