import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BookingRoutingModule } from './booking-routing.module';
import { BookingComponent } from './booking.component';
import { ShareModule } from 'src/app/module/share/share.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [BookingComponent],
  imports: [
    CommonModule,
    FormsModule,
    ShareModule,
    BookingRoutingModule
  ]
})
export class BookingModule { }
