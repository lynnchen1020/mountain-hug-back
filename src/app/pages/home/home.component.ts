import { CampInnOrder, CampOrder, InnOrder, OrderType, SmallCampInnOrder } from './../../enum/order';
import { OrderService } from './../../services/order.service';
import { Component, OnInit } from '@angular/core';
import { tap, zip } from 'rxjs';
import { faCheck, faPen } from '@fortawesome/free-solid-svg-icons';
import { ModalNewService } from 'src/app/services/modal-new.service';
import { ConfirmBoxComponent } from 'src/app/components/modal/confirm-box/confirm-box.component';

interface IPayload {
  order_id: string,
  updateInput: boolean | string
  currentColumnIndex: number,
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  datas = [];
  CampInnOrder = CampInnOrder
  SmallCampInnOrder = SmallCampInnOrder
  CampOrder = CampOrder
  InnOrder = InnOrder
  faCheck = faCheck;
  faPen = faPen;
  isEditMemo = false;
  isSorted = false;
  currentMemo = '';
  typeMemo = '';
  payloadMemo!: IPayload
  OrderType = OrderType;
  modal_ci: any;
  modal_sci: any;
  modal_c: any;
  modal_i: any;
  // modal_nightGoing: any;

  msgTitle: any = {
    [CampInnOrder.IS_PAID_PRE_FEE]: '是否已付訂金',
    [CampInnOrder.IS_NIGHT_GOING]: '是否夜衝',
    [CampInnOrder.IS_SENT_CONFIRM_LETTER]: '是否寄送匯款確認信',
    [CampInnOrder.IS_PAID_REST_FEE]: '是否結清尾款',
    [CampInnOrder.IS_CANCEL_ORDER]: '是否取消訂單',
  }

  orderList: {
    [key: string]: [];
  } = {}

  switchStatus = {
    campInn: {
      is_cancel_order: false,
      is_sort_by_startDate: true,
      is_sort_by_createdDate: false
    },
    smallCampInn: {
      is_cancel_order: false,
      is_sort_by_startDate: true,
      is_sort_by_createdDate: false
    },
    camp: {
      is_cancel_order: false,
      is_sort_by_startDate: true,
      is_sort_by_createdDate: false
    },
    inn: {
      is_cancel_order: false,
      is_sort_by_startDate: true,
      is_sort_by_createdDate: false
    },
  }

  constructor(
    private order: OrderService,
    private modal: ModalNewService
  ) {
  }

  ngOnInit(): void {
    this.getAllOrders();
    this.order.ping().subscribe(data => {
      console.log('pong pong pong', data)
    });
  }

  getColIndex(letter: string) {
    const alphabetMap = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z', 'AA']
    return alphabetMap.indexOf(letter) + 1
  }

  private getAllOrders() {
    zip(
      this.order.getCampInnOrders(),
      this.order.getSmallCampInnOrders(),
      this.order.getCampOrders(),
      this.order.getInnOrders()).pipe(
        tap(x => x)
      ).subscribe(data => {
        this.orderList = {
          campInn: this.sortStartDate(data[0]),
          smallCampInn: this.sortStartDate(data[1]),
          camp: this.sortStartDate(data[2]),
          inn: this.sortStartDate(data[3]),
        }

        console.log('orderList', this.orderList)
      })
  }

  private sortStartDate(arr: any) {
    return arr.sort((a:any, b:any) => {
      return  new Date(a['入住日期']).getTime() - new Date(b['入住日期']).getTime()
    })
  }

  private getUpdatedList(type: string) {
    if(type === 'campInn') {
      const sub = this.order.getCampInnOrders().subscribe(data => {
        this.orderList['campInn'] = this.sortStartDate(data);
        this.isEditMemo = false;
        this.closeModalHandler()
        sub.unsubscribe();
      })
      return;
    }
    if(type === 'smallCampInn') {
      const sub = this.order.getSmallCampInnOrders().subscribe(data => {
        this.orderList['smallCampInn'] = this.sortStartDate(data);
        this.isEditMemo = false;
        this.closeModalHandler()
        sub.unsubscribe();
      })
      return;
    }
    if(type === 'camp') {
      const sub = this.order.getCampOrders().subscribe(data => {
        this.orderList['camp'] = this.sortStartDate(data);
        this.isEditMemo = false;
        this.closeModalHandler()
        sub.unsubscribe();
      })
      return;
    }
    if(type === 'inn') {
      const sub = this.order.getInnOrders().subscribe(data => {
        this.orderList['inn'] = this.sortStartDate(data);
        this.isEditMemo = false;
        this.closeModalHandler()
        sub.unsubscribe();
      })
      return;
    }
  }

  public formatDateTime(str: string) {
    return new Date(str).toLocaleString();
  }

  public formatDate(str: string) {
    return new Date(str).toLocaleDateString();
  }

  public notPaidPreFee(date: string) {
    const ts = new Date().getTime();
    const createdTS = new Date(date).getTime() + (86400000 * 2);
    return ts >= createdTS
  }

  isDue(leaveDate: string) {
    let curTS = new Date().getTime() - 86400000;
    let leaveTS = new Date(leaveDate).getTime();
    return curTS > leaveTS
  }

  // 大包場 toggle checkbox
  toggleCampInnCheckbox(action: string, orderName: string, orderId: string, isChecked: boolean, columnIndex: number) {
    const trans_isChecked = !isChecked ? '是' : '否'

    this.modal_ci = this.modal.open(ConfirmBoxComponent, {
      title: "確認修改?",
      msg: `【${this.msgTitle[action]}】狀態改為: ${trans_isChecked}`,
      order_id: orderId,
      order_name: orderName
    })

    isChecked = !isChecked
    const payload = {
      order_id: orderId,
      updateInput: isChecked,
      currentColumnIndex: columnIndex, //需要改的欄位
    }

    const modalSub = this.modal_ci.onSubmit.subscribe(() => {
      const sub = this.order.updateCampInnOrder(payload).subscribe((data) => {
        const filterOrder: any = this.orderList[OrderType.CAMP_INN].filter(order => {
          return order[CampInnOrder.ORDER_ID] === data.order_id;
        })
        if(action === CampInnOrder.IS_PAID_PRE_FEE) {
          filterOrder[0][CampInnOrder.IS_PAID_PRE_FEE] = data.updateInput
        }
        if(action === CampInnOrder.IS_NIGHT_GOING) {
          filterOrder[0][CampInnOrder.IS_NIGHT_GOING] = data.updateInput
        }
        if(action === CampInnOrder.IS_SENT_CONFIRM_LETTER) {
          filterOrder[0][CampInnOrder.IS_SENT_CONFIRM_LETTER] = data.updateInput
        }
        if(action === CampInnOrder.IS_PAID_REST_FEE) {
          filterOrder[0][CampInnOrder.IS_PAID_REST_FEE] = data.updateInput
        }
        if(action === CampInnOrder.IS_CANCEL_ORDER) {
          filterOrder[0][CampInnOrder.IS_CANCEL_ORDER] = data.updateInput
        }

        this.modal_ci.close()

        if(sub) {
          sub.unsubscribe()
        }
      });
      console.log('確定')
      if(modalSub) {
        modalSub.unsubscribe()
      }
    })
    this.modal_ci.onCancel.subscribe(() => {
      console.log('取消')
    })
    return
  }

  // 小包區 toggle checkbox
  toggleSmallCampInnCheckbox(action: string, orderName: string, orderId: string, isChecked: boolean, columnIndex: number) {
    const trans_isChecked = !isChecked ? '是' : '否'

    this.modal_sci = this.modal.open(ConfirmBoxComponent, {
      title: "確認修改?",
      msg: `【${this.msgTitle[action]}】狀態改為: ${trans_isChecked}`,
      order_id: orderId,
      order_name: orderName
    })

    isChecked = !isChecked
    const payload = {
      order_id: orderId,
      updateInput: isChecked,
      currentColumnIndex: columnIndex, //需要改的欄位
    }

    const modalSub = this.modal_sci.onSubmit.subscribe(() => {
      const sub = this.order.updateSmallCampInnOrder(payload).subscribe((data) => {
        const filterOrder: any = this.orderList[OrderType.SMALL_CAMP_INN].filter(order => {
          return order[SmallCampInnOrder.ORDER_ID] === data.order_id;
        })
        console.log('data', data)
        console.log('this.orderList[OrderType.SMALL_CAMP_INN]', this.orderList[OrderType.SMALL_CAMP_INN])
        if(action === SmallCampInnOrder.IS_PAID_PRE_FEE) {
          filterOrder[0][SmallCampInnOrder.IS_PAID_PRE_FEE] = data.updateInput
        }
        if(action === SmallCampInnOrder.IS_NIGHT_GOING) {
          filterOrder[0][SmallCampInnOrder.IS_NIGHT_GOING] = data.updateInput
        }
        if(action === SmallCampInnOrder.IS_SENT_CONFIRM_LETTER) {
          filterOrder[0][SmallCampInnOrder.IS_SENT_CONFIRM_LETTER] = data.updateInput
        }
        if(action === SmallCampInnOrder.IS_PAID_REST_FEE) {
          filterOrder[0][SmallCampInnOrder.IS_PAID_REST_FEE] = data.updateInput
        }
        if(action === SmallCampInnOrder.IS_CANCEL_ORDER) {
          filterOrder[0][SmallCampInnOrder.IS_CANCEL_ORDER] = data.updateInput
        }

        this.modal_sci.close()

        if(sub) {
          sub.unsubscribe()
        }
      });
      console.log('確定')
      if(modalSub) {
        modalSub.unsubscribe()
      }
    })
    this.modal_sci.onCancel.subscribe(() => {
      console.log('取消')
    })
    return
  }

  // 露營區 toggle checkbox
  toggleCampCheckbox(action: string, orderName: string, orderId: string, isChecked: boolean, columnIndex: number) {
    const trans_isChecked = !isChecked ? '是' : '否'

    this.modal_c = this.modal.open(ConfirmBoxComponent, {
      title: "確認修改?",
      msg: `【${this.msgTitle[action]}】狀態改為: ${trans_isChecked}`,
      order_id: orderId,
      order_name: orderName
    })

    isChecked = !isChecked
    const payload = {
      order_id: orderId,
      updateInput: isChecked,
      currentColumnIndex: columnIndex, //需要改的欄位
    }

    const modalSub = this.modal_c.onSubmit.subscribe(() => {
      const sub = this.order.updateCampOrder(payload).subscribe((data) => {
        console.log('this.orderList[OrderType.CAMP]', this.orderList[OrderType.CAMP])
        const filterOrder: any = this.orderList[OrderType.CAMP].filter(order => {
          console.log('order', order, 'data', data)
          return order[CampOrder.ORDER_ID] === data.order_id;
        })
        if(action === CampOrder.IS_PAID_PRE_FEE) {
          console.log(filterOrder[0])
          filterOrder[0][CampOrder.IS_PAID_PRE_FEE] = data.updateInput
        }
        if(action === CampOrder.IS_NIGHT_GOING) {
          filterOrder[0][CampOrder.IS_NIGHT_GOING] = data.updateInput
        }
        if(action === CampOrder.IS_SENT_CONFIRM_LETTER) {
          filterOrder[0][CampOrder.IS_SENT_CONFIRM_LETTER] = data.updateInput
        }
        if(action === CampOrder.IS_PAID_REST_FEE) {
          filterOrder[0][CampOrder.IS_PAID_REST_FEE] = data.updateInput
        }
        if(action === CampOrder.IS_CANCEL_ORDER) {
          filterOrder[0][CampOrder.IS_CANCEL_ORDER] = data.updateInput
        }

        this.modal_c.close()

        if(sub) {
          sub.unsubscribe()
        }
      });
      console.log('確定')
      if(modalSub) {
        modalSub.unsubscribe()
      }
    })
    this.modal_c.onCancel.subscribe(() => {
      console.log('取消')
    })
    return
  }

  // 宿屋 toggle checkbox
  toggleInnCheckbox(action: string, orderName: string, orderId: string, isChecked: boolean, columnIndex: number) {
    const trans_isChecked = !isChecked ? '是' : '否'

    this.modal_i = this.modal.open(ConfirmBoxComponent, {
      title: "確認修改?",
      msg: `【${this.msgTitle[action]}】狀態改為: ${trans_isChecked}`,
      order_id: orderId,
      order_name: orderName
    })

    isChecked = !isChecked
    const payload = {
      order_id: orderId,
      updateInput: isChecked,
      currentColumnIndex: columnIndex, //需要改的欄位
    }

    const modalSub = this.modal_i.onSubmit.subscribe(() => {
      const sub = this.order.updateInnOrder(payload).subscribe((data) => {
        const filterOrder: any = this.orderList[OrderType.INN].filter(order => {
          return order[InnOrder.ORDER_ID] === data.order_id;
        })
        if(action === InnOrder.IS_PAID_PRE_FEE) {
          filterOrder[0][InnOrder.IS_PAID_PRE_FEE] = data.updateInput
        }
        if(action === InnOrder.IS_NIGHT_GOING) {
          filterOrder[0][InnOrder.IS_NIGHT_GOING] = data.updateInput
        }
        if(action === InnOrder.IS_SENT_CONFIRM_LETTER) {
          filterOrder[0][InnOrder.IS_SENT_CONFIRM_LETTER] = data.updateInput
        }
        if(action === InnOrder.IS_PAID_REST_FEE) {
          filterOrder[0][InnOrder.IS_PAID_REST_FEE] = data.updateInput
        }
        if(action === InnOrder.IS_CANCEL_ORDER) {
          filterOrder[0][InnOrder.IS_CANCEL_ORDER] = data.updateInput
        }

        this.modal_i.close()

        if(sub) {
          sub.unsubscribe()
        }
      });
      console.log('確定')
      if(modalSub) {
        modalSub.unsubscribe()
      }
    })
    this.modal_i.onCancel.subscribe(() => {
      console.log('取消')
    })
    return

  }

  // 修改訂單備註
  updateMemo(type: string, orderId: string, memoText: string, columnIndex: number) {
    const payload = {
      order_id: orderId,
      updateInput: memoText,
      currentColumnIndex: columnIndex, //需要改的欄位
    }
    this.payloadMemo = payload;
    this.typeMemo = type;
    this.isEditMemo = true;
  }

  onUpdateList($event: string) {
    this.getUpdatedList($event)
  }

  private closeModalHandler() {
    if(this.modal_ci) {
      this.modal_ci.close();
    }
    if(this.modal_sci) {
      this.modal_sci.close();
    }
    if(this.modal_c) {
      this.modal_c.close();
    }
    if(this.modal_i) {
      this.modal_i.close();
    }
  }

  public sortByStartDate(orderType: string, data: any, title: string) {
    if(orderType === OrderType.CAMP_INN) {
      this.sortData(data, title, this.switchStatus[OrderType.CAMP_INN].is_sort_by_startDate)

      this.switchStatus[OrderType.CAMP_INN].is_sort_by_startDate = !this.switchStatus[OrderType.CAMP_INN].is_sort_by_startDate

      return
    }

    if(orderType === OrderType.SMALL_CAMP_INN) {
      this.sortData(data, title, this.switchStatus[OrderType.SMALL_CAMP_INN].is_sort_by_startDate)

      this.switchStatus[OrderType.SMALL_CAMP_INN].is_sort_by_startDate = !this.switchStatus[OrderType.SMALL_CAMP_INN].is_sort_by_startDate

      return
    }

    if(orderType === OrderType.CAMP) {
      this.sortData(data, title, this.switchStatus[OrderType.CAMP].is_sort_by_startDate)

      this.switchStatus[OrderType.CAMP].is_sort_by_startDate = !this.switchStatus[OrderType.CAMP].is_sort_by_startDate

      return
    }
    if(orderType === OrderType.INN) {
      this.sortData(data, title, this.switchStatus[OrderType.INN].is_sort_by_startDate)

      this.switchStatus[OrderType.INN].is_sort_by_startDate = !this.switchStatus[OrderType.INN].is_sort_by_startDate

      return
    }

    // this.switchStatus[OrderType.CAMP_INN].sort_by_startDate = !this.switchStatus[OrderType.CAMP_INN].sort_by_startDate


  }

  public sortByCreatedDate(orderType: string, data: any, title: string) {
    if(orderType === OrderType.CAMP_INN) {
      this.sortData(data, title, this.switchStatus[OrderType.CAMP_INN].is_sort_by_createdDate)

      this.switchStatus[OrderType.CAMP_INN].is_sort_by_createdDate = !this.switchStatus[OrderType.CAMP_INN].is_sort_by_createdDate

      return
    }

    if(orderType === OrderType.SMALL_CAMP_INN) {
      this.sortData(data, title, this.switchStatus[OrderType.SMALL_CAMP_INN].is_sort_by_createdDate)

      this.switchStatus[OrderType.SMALL_CAMP_INN].is_sort_by_createdDate = !this.switchStatus[OrderType.SMALL_CAMP_INN].is_sort_by_createdDate

      return
    }

    if(orderType === OrderType.CAMP) {
      this.sortData(data, title, this.switchStatus[OrderType.CAMP].is_sort_by_createdDate)

      this.switchStatus[OrderType.CAMP].is_sort_by_createdDate = !this.switchStatus[OrderType.CAMP].is_sort_by_createdDate

      return
    }
    if(orderType === OrderType.INN) {
      this.sortData(data, title, this.switchStatus[OrderType.INN].is_sort_by_createdDate)

      this.switchStatus[OrderType.INN].is_sort_by_createdDate = !this.switchStatus[OrderType.INN].is_sort_by_createdDate

      return
    }
  }

  private sortData(data: any, title: string, isSorted: boolean) {
    return data.sort((a: any, b: any) => {
      if(isSorted) {
        return new Date(b[title]).getTime() - new Date(a[title]).getTime()
      } else {
        return new Date(a[title]).getTime() - new Date(b[title]).getTime()
      }
    })
  }

}
