import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { StickyDirectiveModule } from 'src/app/directives/sticky-table.directive';
import { ShareModule } from 'src/app/module/share/share.module';


@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
    StickyDirectiveModule,
    ShareModule,
    HomeRoutingModule
  ]
})
export class HomeModule { }
