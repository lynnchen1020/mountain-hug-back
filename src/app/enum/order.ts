export enum OrderType {
  CAMP_INN = 'campInn',
  SMALL_CAMP_INN = 'smallCampInn',
  CAMP = 'camp',
  // INN = 'inn',
  CAMP_A = 'camp_a', // 新的 ordertype
  CAMP_B = 'camp_b', // 新的 ordertype
  CAMP_C = 'camp_c', // 新的 ordertype
  CAMP_D = 'camp_d', // 新的 ordertype
  INN = 'inn', // 新的 ordertype

}

export enum OrderTypeName {
  CAMP_A = '五葉松 A區',
  CAMP_B = '落羽松 L區',
  CAMP_C = 'C雨棚區',
  CAMP_D = 'DXX區',
  INN = '山中宿屋',
}

export enum OrderTypeTentsMax {
  CAMP_A = 4,
  CAMP_B = 5,
  CAMP_C = 0,
  CAMP_D = 0,
  INN = 1,
}

export enum InnOrder {
  CREATED_TIME = '時間戳記',
  ORDER_ID = '訂單編號',
  NAME = '姓名',
  PHONE = '聯絡手機',
  EMAIL = 'Email',
  START_DATE = '入住日期',
  END_DATE = '退房日期',
  STAY_RANGE = '過夜日期',
  ADULTS = '大人',
  CHILDREN = '小孩',
  ADD_BED = '加單人床',
  ADD_BREAKFAST = '供應早餐',
  NIGHTS = '過夜天數',
  PREPAID_FEE = '訂金',
  REST_FEE = '尾款',
  TOTAL_FEE = '總費用',
  IS_PAID_REST_FEE = '是否結清尾款',
  IS_PAID_PRE_FEE = '是否已付訂金',
  CLICKED_IS_PAID_PRE_FEE = '訂金匯款確認日期',
  REQUEST = '其他需求說明',
  ORDER_SOURCE = '訂單類別',
  IS_CANCEL_ORDER = '是否取消訂單',
  IS_SENT_CONFIRM_LETTER = '寄送匯款確認信',
  IS_NIGHT_GOING = '是否夜衝',
  ORDER_MEMO = '訂單備註'
}
export enum CampOrder {
  CREATED_TIME = '時間戳記',
  ORDER_ID = '訂單編號',
  NAME = '姓名',
  PHONE = '聯絡手機',
  EMAIL = 'Email',
  TENT_AREA = '營位',
  START_DATE = '入住日期',
  END_DATE = '離開日期',
  STAY_RANGE = '過夜日期',
  TENTS = '帳數',
  NIGHTS = '過夜天數',
  PREPAID_FEE = '訂金',
  REST_FEE = '尾款',
  TOTAL_FEE = '總費用',
  IS_PAID_REST_FEE = '是否結清尾款',
  IS_PAID_PRE_FEE = '是否已付訂金',
  CLICKED_IS_PAID_PRE_FEE = '訂金匯款確認日期',
  ORDER_SOURCE = '訂單類別',
  IS_CANCEL_ORDER = '是否取消訂單',
  IS_SENT_CONFIRM_LETTER = '寄送匯款確認信',
  IS_NIGHT_GOING = '是否夜衝',
  ORDER_MEMO = '訂單備註'
}

export enum CampInnOrder {
  CREATED_TIME = '時間戳記',
  ORDER_ID = '訂單編號',
  NAME = '姓名',
  PHONE = '聯絡手機',
  EMAIL = 'Email',
  TENT_AREA = '營位',
  START_DATE = '入住日期',
  END_DATE = '退房日期',
  STAY_RANGE = '過夜日期',
  ADD_BED = '加單人床',
  NIGHTS = '過夜天數',
  PREPAID_FEE = '訂金',
  REST_FEE = '尾款',
  TOTAL_FEE = '總費用',
  IS_PAID_REST_FEE = '是否結清尾款',
  IS_PAID_PRE_FEE = '是否已付訂金',
  CLICKED_IS_PAID_PRE_FEE = '訂金匯款確認日期',
  ORDER_SOURCE = '訂單類別',
  IS_CANCEL_ORDER = '是否取消訂單',
  IS_SENT_CONFIRM_LETTER = '寄送匯款確認信',
  IS_NIGHT_GOING = '是否夜衝',
  ORDER_MEMO = '訂單備註'
}

export enum SmallCampInnOrder {
  CREATED_TIME = '時間戳記',
  ORDER_ID = '訂單編號',
  NAME = '姓名',
  PHONE = '聯絡手機',
  EMAIL = 'Email',
  START_DATE = '入住日期',
  END_DATE = '離開日期',
  STAY_RANGE = '過夜日期',
  ADD_BED = '加單人床',
  NIGHTS = '過夜天數',
  PREPAID_FEE = '訂金',
  REST_FEE = '尾款',
  TOTAL_FEE = '總費用',
  IS_PAID_REST_FEE = '是否結清尾款',
  IS_PAID_PRE_FEE = '是否已付訂金',
  CLICKED_IS_PAID_PRE_FEE = '訂金匯款確認日期',
  ORDER_SOURCE = '訂單類別',
  IS_CANCEL_ORDER = '是否取消訂單',
  IS_SENT_CONFIRM_LETTER = '寄送匯款確認信',
  IS_NIGHT_GOING = '是否夜衝',
  ORDER_MEMO = '訂單備註'
}
