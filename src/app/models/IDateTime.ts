export interface IDateTime {
  toString(): string;
}

export interface IDate {
  year: number;
  month: number;
  day: number;
}
