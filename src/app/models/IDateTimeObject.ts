export interface IDateTimeObject {
  year: number;
  month: number;
  day: number;
  date: number;
  hours: number;
  minutes: number;
  seconds: number;
}
